package com.sourav.bsproject.shared.retrofit

import com.sourav.bsproject.repositoryList.data.RepoList
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface APIFlow {
    suspend fun getRepositoriesFlow(searchQ: String, sortBy: String, perPage: Int): Flow<RepoList>
}

class APIFlowImpl (private val apiService: APIService): APIFlow {
    override suspend fun getRepositoriesFlow(searchQ: String, sortBy: String, perPage: Int) = flow {
        emit(apiService.getRepositories(searchQ,sortBy,perPage))
    }

}