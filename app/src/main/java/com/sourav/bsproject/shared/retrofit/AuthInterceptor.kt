package com.sourav.bsproject.shared.retrofit

import com.sourav.bsproject.App
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val newRequest = originalRequest.newBuilder()
            .header("package", App.context.packageName)
            .build()

        return chain.proceed(newRequest)
    }
}