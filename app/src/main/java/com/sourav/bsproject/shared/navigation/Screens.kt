package com.sourav.bsproject.shared.navigation

enum class Screens(val route: String) {
    RepositoryListScreen("repo_list_screen"),
    RepositoryDetails("repo_details"),
}