package com.sourav.bsproject.shared.retrofit

import com.sourav.bsproject.repositoryList.data.RepoList
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {

    @GET("search/repositories")
    suspend fun getRepositories(
        @Query("q") searchQuery: String,
        @Query("sort") sortby: String,
        @Query("per_page") perPage: Int
    ): RepoList
}