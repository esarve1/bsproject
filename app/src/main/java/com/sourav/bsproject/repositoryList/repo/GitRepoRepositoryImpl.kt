package com.sourav.bsproject.repositoryList.repo

import com.sourav.bsproject.repositoryList.data.RepoList
import com.sourav.bsproject.shared.retrofit.APIFlow
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GitRepoRepositoryImpl @Inject constructor(private val apiFlow: APIFlow) : GitRepoRepository {
    override suspend fun getGitRepositoriesAsFlow(searchQ: String, sortBy: String, perPage: Int): Flow<RepoList> {
        return apiFlow.getRepositoriesFlow(searchQ, sortBy,perPage)
    }
}