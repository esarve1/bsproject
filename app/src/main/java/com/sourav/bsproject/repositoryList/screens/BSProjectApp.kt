package com.sourav.bsproject.repositoryList.screens

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.sourav.bsproject.repositoryList.viewmodel.GitRepositoryVM
import com.sourav.bsproject.shared.navigation.Screens

@Composable
fun BSProjectAPP(){
    Surface (
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background
    ){
        val viewModel : GitRepositoryVM = hiltViewModel()
        val navController = rememberNavController()
        val state by viewModel.state.collectAsStateWithLifecycle()
        NavHost(navController = navController, startDestination = Screens.RepositoryListScreen.route) {
            composable(Screens.RepositoryListScreen.route) {
                GitRepositoryListScreen(
                    state = state,
                    onItemSelected = {
                        viewModel.setItem(it)
                        navController.navigate(Screens.RepositoryDetails.route)
                    }
                )
            }

            composable(Screens.RepositoryDetails.route) {
                GitRepositoryDetails(
                    state = state,
                )
            }
        }
    }
}