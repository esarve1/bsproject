package com.sourav.bsproject.repositoryList.repo

import com.sourav.bsproject.repositoryList.data.RepoList
import kotlinx.coroutines.flow.Flow

interface GitRepoRepository {
    suspend fun getGitRepositoriesAsFlow(searchQ: String, sortBy: String, perPage: Int): Flow<RepoList>
}