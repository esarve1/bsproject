package com.sourav.bsproject.repositoryList.repo

import com.sourav.bsproject.repositoryList.repo.GitRepoRepository
import com.sourav.bsproject.repositoryList.repo.GitRepoRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class GitRepoRepositoryProvider {

    @Singleton
    @Binds
    abstract fun provideRepository(
        currencyRepoImpl: GitRepoRepositoryImpl,
    ): GitRepoRepository
}