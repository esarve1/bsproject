package com.sourav.bsproject.repositoryList.viewmodel

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sourav.bsproject.repositoryList.data.Items
import com.sourav.bsproject.repositoryList.repo.GitRepoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.Date
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class GitRepositoryVM @Inject constructor(private val gitRepoRepository: GitRepoRepository) :  ViewModel(){
    private val _state = MutableStateFlow(ScreenState())
    val state = _state.asStateFlow()

    init {
        getRepositoryLists()
    }
    private fun getRepositoryLists (){
        _state.update {
            it.copy(
                isLoading = true
            )
        }
        viewModelScope.launch {
            gitRepoRepository.getGitRepositoriesAsFlow("Android", "stars", 15)
                .flowOn(Dispatchers.IO)
                .catch {  }
                .collect{
                    _state.update { st ->
                        st.copy(
                            repoList = it.items,
                            isLoading = false
                        )
                    }
                }
        }


    }

    fun setItem(items: Items?){
        _state.update {
            it.copy(
                items = items
            )
        }
    }

//    fun parseDate(time: String?): String? {
//        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS")
//        val outputPattern = "dd-MMM-yyyy h:ss"
//        var formatter: DateTimeFormatter? = null
//        val date = "2021-11-03T14:09:31.135Z" // your date string
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX") // formatter
//            val dateTime: ZonedDateTime = ZonedDateTime.parse(date, parser) // date object
//            val formatter2: DateTimeFormatter =
//                DateTimeFormatter.ofPattern("EEEE, MMM d : HH:mm") // if you want to convert it any other format
//            Log.e("Date", "" + dateTime.format(formatter2))
//        }
//        return str
//    }

}

data class ScreenState(
    val repoList: List<Items> = emptyList(),
    val items: Items? = null,
    val isLoading: Boolean = false
)